// Задание
// Реализовать функцию для подсчета n-го (обобщенного) числа Фибоначчи.
//
// Технические требования:
// Написать функцию для подсчета n-го обобщенного числа Фибоначчи.
// Аргументами на вход будут три числа - F0, F1, n, где F0, F1 - первые два числа
// последовательности (могут быть любыми целыми числами), n - порядковый номер числа Фибоначчи, которое надо найти.
// Последовательнось будет строиться по следующему правилу F2 = F0 + F1, F3 = F1 + F2 и так далее.
// Считать с помощью модального окна браузера число, которое введет пользователь (n).
// С помощью функции посчитать n-е число в обобщенной последовательности Фибоначчи и вывести его на экран.
// Пользователь может ввести отрицательное число - результат надо посчитать по такому же правилу (F-1 = F-3 + F-2).

// Solution:
console.log(` --- Задача по теме "Фибоначчи". Рекурсия. Итеративный процесс --- `);

function fib(n, F_1, F_2) {
    if (Math.abs(n) === 1) {
        return F_1;
    }
    if (Math.abs(n) === 2) {
        return Math.pow(-1, n + 1) * F_2;
    }

    function F_n(n, i, a, b) {
        if (i === Math.abs(n)) {
            return a * F_1 + b * F_2;
        }
        return F_n(n, i + 1, b, a + b);
    }

    if (n >= 0) {
        return F_n(n, 3, 1, 1);
    }
    if (n < 0) {
        return Math.pow(-1, n + 1) * F_n(n, 3, 1, 1);
    }
}
let F = fib(Number(prompt(`Please, enter sequence number to count Fibonacci number`, '')), 1, 1);
document.write(`Your Fibonacci number is ${F}.`);

/*console.log(` --- Задача по теме "Фибоначчи". Без рекурсии. С применением цикла "for" --- `);

function fib(n, F_1, F_2) {
    let sum;
    if (Math.abs(n) === 1) {
            return F_1;
        }
        if (Math.abs(n) === 2) {
            return Math.pow(-1, n + 1) * F_2;
        }
    if (n > 0) {
        let i = 3, a = 1, b = 1, c = 1;
        for (i; i <= n; i++) {
            sum = a * F_1 + b * F_2;
            c = a + b;
            a = b;
            b = c;
        }
        return sum;
    }
    if (n < 0) {
        let sum, i = 3, a = 1, b = 1, c;
        for (i; i <= Math.abs(n); i++) {
            sum = a * F_1 + b * F_2;
            c = a + b;
            a = b;
            b = c;
        }
        return Math.pow(-1, n + 1) * sum;
    }
}
let F = fib(Number(prompt(`Please, enter sequence number to count Fibonacci number`, '')), 1, 1);
document.write(`Your Fibonacci number is ${F}.`);*/
